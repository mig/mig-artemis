# find Artemis information from the "sources" file
%define artemis_url %((egrep '/(activemq|apache)-artemis/' %{_sourcedir}/sources || \
                       egrep '/(activemq|apache)-artemis/' %{_builddir}/sources  || \
                       echo "x unknown") 2>/dev/null | perl -ane 'print("$F[1]")')
%define artemis_tar %(basename %{artemis_url})
%define artemis_version %(echo %{artemis_tar} | perl -pe 's/^apache-artemis-//; s/-bin\\.tar\\.gz$//')
%define is_snapshot %(echo %{artemis_version} | perl -ne 'print /-\\d{8}\\.\\d{6}-\\d+$/ ? 1 : 0')

# deduce rpm and tar information
%if %{is_snapshot}
%define rpm_release %(echo %{artemis_version} | perl -pe 's/.+-(\\d{8}\\.\\d{6})-(\\d+)$/0.$1.$2/')
%define rpm_version %(echo %{artemis_version} | perl -pe 's/-\\d{8}\\.\\d{6}-\\d+$//')
%define tar_version %{rpm_version}-SNAPSHOT
%else
%define rpm_release 1
%define rpm_version %{artemis_version}
%define tar_version %{artemis_version}
%endif

# installation settings
%define artemis_share   /usr/share/artemis
%define artemis_home    %{artemis_share}-%{artemis_version}
%define artemis_cltname artemis-jms-client-all
%define artemis_cltjar  %{_javadir}/%{artemis_cltname}.jar

# select the right Linux "architecture directory"
%define archdir unknown
%ifarch i686
%define archdir linux-i686
%endif
%ifarch x86_64
%define archdir linux-x86_64
%endif

# disable brp-java-repack-jars
%define __jar_repack %{nil}

# disable debuginfo generation
%global debug_package %{nil}

Summary:	AcitveMQ Artemis
Name:		mig-artemis
Version:	%{rpm_version}
Release:	%{rpm_release}%{?dist}
License:	Apache-2.0
Group:		System
Source0:	%{artemis_tar}
Source1:	artemis
Source2:	service
Source3:	sources
URL: 		https://activemq.apache.org/artemis/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:	perl
Requires:	java >= 0:1.7.0

%description
Apache ActiveMQ Artemis is an open source project to build a multi-protocol,
embeddable, very high performance, clustered, asynchronous messaging system.

%package client
Summary:	ActiveMQ Artemis client jar
Group:		System
Provides:	%{artemis_cltname}.jar

%description client
Apache ActiveMQ Artemis' client jar with all the dependencies.

%prep
%setup -q -n apache-artemis-%{tar_version}

%build
# add our own artemis script but keep the old one as "artemis-dist"
mv bin/artemis bin/artemis-dist
install -m 0755 %{SOURCE1} bin/
# add our own service script
install -m 0755 %{SOURCE2} bin/
# move the relevant libartemis-native-*.so to lib
mv bin/lib/%{archdir}/libartemis-native-*.so lib/
rm -fr bin/lib
# remove the files we do not need
rm -fr bin/artemis.cmd examples lib/activemq-client-*.jar

%install
rm -fr %{buildroot}
mkdir -p %{buildroot}%{artemis_home}
mv * %{buildroot}%{artemis_home}
# move the client jar away
mkdir -p %{buildroot}%{_javadir}
mv %{buildroot}%{artemis_home}/lib/client/%{artemis_cltname}-*.jar \
 %{buildroot}%{_javadir}/%{artemis_cltname}-%{artemis_version}.jar
rmdir %{buildroot}%{artemis_home}/lib/client

%clean
rm -fr %{buildroot}

%post
[ -e %{artemis_share} ] && rm -f %{artemis_share}
ln -s artemis-%{artemis_version} %{artemis_share}

%post client
[ -e %{artemis_cltjar} ] && rm -f %{artemis_cltjar}
ln -s %{artemis_cltname}-%{artemis_version}.jar %{artemis_cltjar}

%postun
rm -f %{artemis_share}
cd /usr/share
last=`ls -dt artemis-* 2>/dev/null | head -1`
[ "x$last" != "x" ] && ln -s $last %{artemis_share} || true

%postun client
rm -f %{artemis_cltjar}
cd %{_javadir}
last=`ls -dt %{artemis_cltname}-*.jar 2>/dev/null | head -1`
[ "x$last" != "x" ] && ln -s $last %{artemis_cltjar} || true

%files
%defattr(-,root,root,-)
%{artemis_home}

%files client
%defattr(-,root,root,-)
%{_javadir}/*.jar
