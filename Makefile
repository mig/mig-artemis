#
# download and check the files declared in the "sources" file
#

.PHONY: sources download check clean tag

sources: download check

download:
	@cat sources | while read line; do \
	    set $$line; \
	    curl -O -R -S -f -s $$2 ; \
	done

check:
	@cat sources | while read line; do \
	    set $$line; \
	    echo "$$1  `basename $$2`" | md5sum -c; \
	done

clean:
	@cat sources | while read line; do \
	    set $$line; \
	    rm -fv `basename $$2`; \
	done

tag:
	@tag=`perl -ne 'print("v$$1\n") if m{/apache-artemis-(.+)-bin\.(tar\.gz|zip)$$}' sources`; \
	seen=`git tag -l | grep -Fx $$tag`; \
	if [ "x$$seen" = "x" ]; then \
	    set -x; \
	    git tag $$tag; \
	    git push --tags; \
	else \
	    echo "already tagged with $$tag"; \
	fi
